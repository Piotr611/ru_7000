/******************************************************************************
 * 
 * Controller Area Network (CAN) Demo-Application
 * Atmel AVR with Microchip MCP2515 
 * 
 * Copyright (C) 2005 Martin THOMAS, Kaiserslautern, Germany
 * <eversmith@heizung-thomas.de>
 * http://www.siwawi.arubi.uni-kl.de/avr_projects
 *
 *****************************************************************************
 *
 * File    : can.c
 * Version : 0.9
 * 
 * Summary : Interface between application and CAN-Hardware
 *           ("middle tier", "abstraction layer")
 *
 *****************************************************************************/

#include <string.h>

#include "can.h"
#include "mcp2515.h"


uint8_t can_init(uint8_t speedset)
{
	uint8_t res;
	
	res = mcp2515_init(speedset);
	
	if (res == MCP2515_OK) {PORTD|=0x04;GICR|=0x40;MCUCR|=0x0;return CAN_OK;}
	else return CAN_FAILINIT;
}

void can_initMessageStruct(CanMessage* msg) {	memset(msg,0,sizeof(CanMessage));}

uint8_t can_sendMessage(const CanMessage* msg)
{
GICR&=0xBF;
	uint8_t res, txbuf_n;
	uint8_t timeout = 0;
	uint8_t can_been_used_flag=GICR&0x40; //
	can_time_busy=250;
	
	do {
		res = mcp2515_getNextFreeTXBuf(&txbuf_n); // info = addr.
		if (can_time_busy==0) timeout = 1;
	} while (res == MCP_ALLTXBUSY && !timeout);
	
	if (!timeout) {
		mcp2515_write_canMsg( txbuf_n, msg);
		mcp2515_start_transmit( txbuf_n );
		if (!can_been_used_flag)
			GICR|=0x40;
		return CAN_OK;
	}
	else {
		if (!can_been_used_flag)
			GICR|=0x40;
		return CAN_FAILTX;
	}
}



uint8_t can_readMessage(CanMessage *msg)
{
	uint8_t stat, res;
	
	stat = mcp2515_readStatus();
	
	if ( stat & MCP_STAT_RX0IF ) {
		// Msg in Buffer 0
		mcp2515_read_canMsg( MCP_RXBUF_0, msg);
		mcp2515_modifyRegister(MCP_CANINTF, MCP_RX0IF, 0);
		res = CAN_OK;
	}
	else if ( stat & MCP_STAT_RX1IF ) {
		// Msg in Buffer 1
		mcp2515_read_canMsg( MCP_RXBUF_1, msg);
		mcp2515_modifyRegister(MCP_CANINTF, MCP_RX1IF, 0);
		res = CAN_OK;
	}
	else {
		res = CAN_NOMSG;
	}	
	
	return res;
}

/* returns either 
 #define CAN_MSGAVAIL   (3) - a message has been received
 #define CAN_NOMSG      (4) - no new message
*/
uint8_t can_checkReceive(void)
{	uint8_t res;
	
	res = mcp2515_readStatus(); // RXnIF in Bit 1 and 0
	if ( res & MCP_STAT_RXIF_MASK ) {
		return CAN_MSGAVAIL;
	}
	else {
		return CAN_NOMSG;
	}
}

/* checks Controller-Error-State, returns CAN_OK or CAN_CTRLERROR 
   only errors (and not warnings) lead to "CTRLERROR" */

uint8_t can_checkError(void)
{	uint8_t eflg = mcp2515_readRegister(MCP_EFLG);
	if ( eflg & MCP_EFLG_ERRORMASK ) {
		return CAN_CTRLERROR;
	}
	else {
		return CAN_OK;
	}
}


uint8_t can_testTransmit(const uint8_t ext, const uint8_t data2)
{
	CanMessage msg;
	
	can_initMessageStruct(&msg);
	msg.identifier = CANDEFAULTIDENT;
	msg.extended_identifier = ext;
	msg.dlc = 3;
	msg.dta[0] = 'M';
	msg.dta[1] = 'T';
	msg.dta[2] = data2;
	
	return can_sendMessage(&msg);
}

#include <avr/eeprom.h>

//������������� ����������������� ������
#define ee_reduc_vom 		0x08
#define ee_porog_vom 		0x10
#define ee_diametr_kolesa 	0x18
#define ee_metki_koleso		0x20
#define ee_metki_vom		0x28
#define ee_vid_udobrenie	0x30
#define ee_rashod_kg_ga		0x38
#define ee_shirina_zahvata	0x40
#define ee_shirina_num		0x48
#define ee_nasyp_massa		0x50
#define ee_vysota_naveski	0x58
#define ee_kalibr_coef		0x60

#define ee_emul_speed		0x68
#define ee_emul_speed_num	0x70
/*
#define ee_max_posit_left	0x78
#define ee_min_posit_left	0x80
#define ee_max_posit_right	0x88
#define ee_min_posit_right	0x90
*/
//#define ee_metki_obr_lenta	0x78
//#define ee_koefficient_lenta 0x80
#define ee_bunker_MIN		0x88
#define ee_bunker_MAX		0x90

#define ee_bunker_zagruzka	0x98

#define ee_coef_lenta	 	0xA0
#define ee_metki_lenta		0xA8
#define ee_diametr_lenta	0xB0
#define ee_low_level_bunker_kg	0xB8
#define ee_low_level_bunker_sensor	0xC0
#define ee_vom				0xC8
#define ee_popr_lenta		0xD0 // ������������ ����������� �����. ������������ ������ ��� ����� (<0.10) coef_lenta
#define ee_calib_coeff		0xD8

#define ee_square_w			0x180	//��������� ������������ �������
#define ee_square_s			0x1A0	//��������� ������������ �������
#define ee_kg1_w			0x1C0	//��������� ����� ���������������� ��������� ������ 1 ��
#define ee_kgramm_b			0x240	//��������� ����� ���������������� ��������� ������ 1 ��

volatile unsigned char eeprom_busy=0;
void PROBEG_CLR(unsigned int addr);

unsigned int read_config(unsigned int addres,unsigned int max,unsigned int undo);
unsigned long PROBEG_RD(unsigned int addr);


void read_configuration(void){
reduc_vom=read_config(ee_reduc_vom,20,1);
porog_vom=read_config(ee_porog_vom,1000,100);					//��������� �������� ���� ������ �������� ���������� ������� ��������� ��������������
diametr_kolesa=read_config(ee_diametr_kolesa,3000,1300);		//������� ������
metki_koleso=read_config(ee_metki_koleso,100,6);				//���������� ����� �� ������
metki_vom=read_config(ee_metki_vom,100,2);						//���������� ����� �� ���
bunker_zapolnenie=read_config(ee_bunker_zagruzka,MAX_ZAGRUZKA,300);			//��������� ���������� �������
nasyp_massa=read_config(ee_nasyp_massa,150,100);
low_level_bunker_kg=read_config(ee_low_level_bunker_kg,1000,200);	// ����� ������� ������� ���������� �������
low_level_bunker_sensor=read_config(ee_low_level_bunker_sensor,2,1);	// ����� ������� ������� ���������� �������

bunker_MIN = read_config(ee_bunker_MIN,1000,50);
bunker_MAX = read_config(ee_bunker_MAX,10000,7000);

//vom= 1001; 
//eep_word (ee_vom,vom);
vom= read_config(ee_vom,1200,900);

//max_posit_left=read_config(ee_max_posit_left,110,100);
//min_posit_left=read_config(ee_min_posit_left,110,0);
//max_posit_right=read_config(ee_max_posit_right,110,100);
//min_posit_right=read_config(ee_min_posit_right,110,0);
kalibr_coef=read_config(ee_kalibr_coef,9999,100);

calib_coeff=read_config(ee_calib_coeff,200,100);				// ���������� ��������������
if (calib_coeff < 50)
	{
	calib_coeff = 50;
	eep_word(ee_calib_coeff,calib_coeff);
	}

vid_udobrenie=read_config(ee_vid_udobrenie,100,3);				//��������������� ��� ��������� (�������)
rashod_kg_ga=read_config(ee_rashod_kg_ga,2000,250);				//��������� ������� ������
shirina_zahvata=read_config(ee_shirina_zahvata,50,10);			//������� ������ �������
//shirina_zahvata=20;
shirina_num=read_config(ee_shirina_num,50,5);
vysota_naveski=read_config(ee_vysota_naveski,161,30);


//coef_lenta=   read_config(ee_coef_lenta,9999,2);			
popr_lenta=   read_config(ee_popr_lenta,9999,119);			//������� ������ �����
metki_lenta=  read_config(ee_metki_lenta,100,5);				//���������� ����� �� ������
diametr_lenta=read_config(ee_diametr_lenta,9999,141);			//������� ������ �����
metki_lenta = read_config(ee_metki_lenta,100,6);       //����� �� ������ �����
coef_lenta = read_config(ee_coef_lenta,200,100);	//����������� ���� �����

emul_speed=read_config(ee_emul_speed,200,0);
emul_speed_num=read_config(ee_emul_speed_num,100,0);
//speed_control=100;										//��������������� ��������*10 ��/�
//work_vom=540;											//������� ���� ������ ��������
//work_disk=1;											//�������������� ����
//vysota_naveski=60;										//��������������� ������ �������
//lopatka_1_1=0xc3;										//��������� ������ �������
//lopatka_1_2=0xc1;										//��������� ������ ������� �������
//lopatka_2_1=0xa4;										//��������� ������ �������
//lopatka_2_2=0xa3;										//��������� ������ ������� �������



gramm_w=PROBEG_RD(ee_kg1_w)*10;							//��������� ���������� ����������� ��������
square_w=PROBEG_RD(ee_square_w);
square_s=PROBEG_RD(ee_square_s);



init_count();
}


#define eeprom_rb(addr) eeprom_read_byte ((uint8_t *)(addr))
#define eeprom_rw(addr) eeprom_read_word ((uint16_t *)(addr))
#define eeprom_wb(addr,val) eeprom_write_byte ((uint8_t *)(addr), (uint8_t)(val))


void test_eep (unsigned int addres){
unsigned char dat0,dat1,dat2;
while (!eeprom_is_ready());
dat0=  eeprom_rb ((int)addres);
dat1=  eeprom_rb (addres+2);
dat2=  eeprom_rb (addres+4);

if((dat0==dat1)&(dat0!=dat2)) {while (!eeprom_is_ready());eeprom_write_byte((uint8_t *)(addres+4),dat0);};
if((dat0==dat2)&(dat0!=dat1)) {while (!eeprom_is_ready());eeprom_write_byte((uint8_t *)(addres+2),dat0);};
if((dat1==dat2)&(dat0!=dat1)) {while (!eeprom_is_ready());eeprom_write_byte((uint8_t *)addres,dat1);};

}

void eep_wr (unsigned int addres,unsigned char dat){
eeprom_busy_wait();eeprom_wb((int)addres,dat);
eeprom_busy_wait();eeprom_wb(addres+2,dat);
eeprom_busy_wait();eeprom_wb(addres+4,dat);
}


unsigned char read_byte_ee (unsigned int addres){
test_eep (addres);
return(eeprom_rb((int)addres));}


void eep_word (unsigned int addres,unsigned int dat){
eep_wr(addres+1,dat>>8);eep_wr(addres,dat);
}

unsigned int read_word (unsigned int addres){
return((((int)read_byte_ee(addres+1))<<8)+read_byte_ee(addres));};

unsigned int read_config(unsigned int addres,unsigned int max,unsigned int undo){
unsigned int x=read_word(addres);if (x>max) {x=undo;eep_word(addres,x);};
return(x);
}



unsigned long PROBEG_RD(unsigned int addr)  //����� �������� �������
{unsigned int probeg[8];
unsigned int max_probeg=0;
eeprom_busy=1; eeprom_read_block(&probeg[0],(void*)addr,16); eeprom_busy=0;
unsigned char n;
for (n=0;n<8;n++) {if (probeg[n]==0xffff) probeg[n]=0; if (probeg[n]>=max_probeg) max_probeg=probeg[n];};
for (n=7;n!=0;n--) if (probeg[n]==max_probeg) break;
if (max_probeg) max_probeg--; else n=0;
return ((long)max_probeg*8+n);
}

void PROBEG_WR(unsigned int addr,long prob)
{if (prob>499999) {PROBEG_CLR(addr);return;};
 
 unsigned int sm=(prob>>3)+1;
 unsigned char sm1=prob;sm1&=7;sm1=sm1<<1;
		eeprom_busy=1;
		while (!eeprom_is_ready());
		eeprom_write_word((void*)addr+sm1,sm);
		eeprom_busy=0;		
}


void PROBEG_CLR(unsigned int addr){
unsigned char n;
for (n=0;n<16;n+=2)   {eeprom_busy=1;
						while (!eeprom_is_ready());
						eeprom_write_word((void*)addr+n,0);
						eeprom_busy=0;
						};
};

void Write_probeg(unsigned int addr,long prob)
{ unsigned int sm=(prob>>3)+1;
 unsigned char sm1=prob;sm1&=7;sm1=sm1<<1;
		while (!eeprom_is_ready());
		eeprom_write_word((void*)addr+sm1,sm);
}


//����������� ������� �� 1 ��� ���������� �������� �������� ������ �������� ��������
//���������� 1 ���� ���� ����������
unsigned char PROBEG_INC (unsigned int addr,unsigned long predel,unsigned long *tem)
{if ((*tem>predel)&(eeprom_busy==0)) {*tem%=(predel+1);
										unsigned long z=PROBEG_RD(addr);
										z++;
										PROBEG_WR(addr,z);
										return(1);};
return(0);
}

void clear_probeg(unsigned int addr){

for (unsigned char n=0;n<16;n+=2) {while (!eeprom_is_ready());eeprom_write_word((void*)addr+n,0);};
};
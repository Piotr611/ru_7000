volatile unsigned char rx_d;
volatile unsigned char last_id;
volatile unsigned char last_last_id;

#define IDS_A {msg_tx.extended_identifier = 1;                      \
			msg_tx.identifier = 0x18FF0000 + (IDS<<8) + CAN_ID_DA; \
			msg_tx.dlc = 8;      \
                                        \
			can_sendMessage(&msg_tx);\
			}
#define IDC_A {msg_tx.extended_identifier = 1;                      \
			msg_tx.identifier = 0x10E80000 + ((msg_rx.identifier&0x000000FF)<<8) + CAN_ID_DA; \
			msg_tx.dlc = 8;      \
                                        \
			msg_tx.dta[0] = IDC;\
			msg_tx.dta[1] = 0;\
			msg_tx.dta[2] = msg_tx.dta[3] = msg_tx.dta[4] = msg_tx.dta[5] = msg_tx.dta[6] = msg_tx.dta[7] = 0xFF;  \
			can_sendMessage(&msg_tx);\
			}
CanMessage msg;		//�������� ��������� ����������� ����������
CanMessage msg_tx_int;		//�������� ��������� ������� ����������			
CanMessage msg_rx;
CanMessage msg_tx;
volatile unsigned char msg_rx_ready_flag;

void can_analize (void)
{
if (msg_rx_ready_flag)
	{
	unsigned int IDS = msg_rx.dta[0];
	if ((msg_rx.identifier>>8) == (0x18EA00 + CAN_ID_DA))   // ������ REQ
		{
		switch (IDS)
			{
			case 0x20:
				*(unsigned int *)(&msg_tx.dta[0]) = (unsigned int)metki_lenta; 
				*(unsigned int *)(&msg_tx.dta[2]) = (unsigned int)diametr_lenta;
				*(unsigned int *)(&msg_tx.dta[4]) = (unsigned int)coef_lenta;
				*(unsigned int *)(&msg_tx.dta[6]) = (unsigned int)	metki_koleso;
				
				IDS_A;
			break;
			
			case 0x21:
				*(unsigned int *)(&msg_tx.dta[0]) = 				diametr_kolesa;
				*(unsigned int *)(&msg_tx.dta[2]) = (unsigned int)	metki_vom;
				*(unsigned int *)(&msg_tx.dta[4]) = 				porog_vom;
				*(unsigned int *)(&msg_tx.dta[6]) = 				reduc_vom;
				
				IDS_A;
			break;	
			
			case 0x22:
				*(unsigned int *)(&msg_tx.dta[0]) = 				0xFFFF;
				*(unsigned int *)(&msg_tx.dta[2]) = 				0xFFFF;
				*(unsigned int *)(&msg_tx.dta[4]) = 				0xFFFF;
				*(unsigned int *)(&msg_tx.dta[6]) = 				0xFFFF;
				
				IDS_A;
			break;	
				
						
			case 0x23: //��������� ��� ����
				*(unsigned long *)(&msg_tx.dta[0]) = (unsigned long)	square_s;
				*(unsigned long *)(&msg_tx.dta[4]) = (unsigned long)	way_s;
				
				IDS_A;
			break;
			
			
			case 0x24: //��������� ��� ����
				*(unsigned long *)(&msg_tx.dta[0]) = (unsigned long)	time_s + time_sec_ws/60;
				*(unsigned long *)(&msg_tx.dta[4]) = (unsigned long)	kgramm_s + gramm/10000;
				
				IDS_A;
			break;
			
			
			case 0x25: // ������� ��������
				/*
				*(unsigned int *)(&msg_tx.dta[0]) = taho[1]; //��������  
				//*(unsigned int *)(&msg_tx.dta[2]) = taho[0]; //���
				*(unsigned int *)(&msg_tx.dta[2]) = taho_chanel[0].taho_rpm; //BOM
				*(unsigned int *)(&msg_tx.dta[4]) = taho[2]; //�������� �����
				*(unsigned int *)(&msg_tx.dta[6]) = taho[3];
				*/
				*(unsigned int *)(&msg_tx.dta[0]) = taho_chanel[0].pulse_sec;//������� ��������
				*(unsigned int *)(&msg_tx.dta[2]) = taho_chanel[1].pulse_sec;//������� ���
				*(unsigned int *)(&msg_tx.dta[4]) = taho_chanel[3].pulse_sec;//������� �������� �����
				//*(unsigned int *)(&msg_tx.dta[4]) = taho_chanel[2].pulse_sec;//������� �������� �����				
				IDS_A;
			break;
			
			case 0xEE:  // ������ �� � ������
				msg_tx.dta[0] = 0;
				msg_tx.dta[1] = 5;
				msg_tx.dta[2] = 4;
				msg_tx.dta[3] = 1; // �����
				
				msg_tx.dta[4] = 0;
				msg_tx.dta[5] = 0;
				msg_tx.dta[6] = 1;
				msg_tx.dta[7] = 1;
				IDS_A;
			break;
			}			
		}	
	msg_rx_ready_flag = 0;
	}
}

SIGNAL  (SIG_INTERRUPT0) 
{
GIFR|=0x40;
GICR&=~(1<<6); // ����� 0x40
if (can_checkReceive()!=CAN_NOMSG) 
	{

	can_readMessage(&msg);
	sei();


	if (msg_rx_ready_flag == 0)
		{
		msg_rx.identifier = msg.identifier;
		for (unsigned char i = 0; i < 8; i++) msg_rx.dta [i] = msg.dta[i];
		msg_rx_ready_flag = 1;
		}
	switch (msg.identifier)
		{
		case	0x18EB5600: {*(unsigned int *)(&msg_tx_int.dta[0]) = 0;//������� ��������
					 *(unsigned int *)(&msg_tx_int.dta[2]) = 0;//������� ���
					 *(unsigned int *)(&msg_tx_int.dta[4]) =0;//������� �������� �����
					 *(unsigned int *)(&msg_tx_int.dta[6]) =0;//����� �����
					};
					msg_tx_int.extended_identifier = 1;
					msg_tx_int.identifier = 0x18FF1234;
					msg_tx_int.dlc = 8;
					can_sendMessage(&msg_tx_int);
		break;
		
		case	0x18FF1050: 
				if (msg.dta[0])
					{
					GPS_actual_data = 3;
					speed_now_GPS = *(unsigned int *)(&msg.dta[2]);
					
					msg_tx_int.extended_identifier = 1;
					msg_tx_int.identifier = 0x18FF5050;
					msg_tx_int.dlc = 8;
					
					*(unsigned int *)(&msg_tx_int.dta[0]) = *(unsigned int *)(&msg.dta[2]);
					*(unsigned int *)(&msg_tx_int.dta[2]) = 0;//������� ���
					 *(unsigned int *)(&msg_tx_int.dta[4]) =0;//������� �������� �����
					 *(unsigned int *)(&msg_tx_int.dta[6]) =0;//����� �����
					//can_sendMessage(&msg_tx_int);
					}
		break;
		
		};
		
	can_analize ();
	
	unsigned char IDC = msg.dta[0];
	if ((msg.identifier>>8) == (0x0CEA00 + CAN_ID_DA))   // ������ CMD
		{
		switch (IDC)
			{
			case 0x0A:  // �������� �����
				if ( (msg.dta[1] == 0) || (msg.dta[1] == 1) )
					reductor_right_on = msg.dta[1];
				if (msg.dta[1] == 2)	
					reductor_right_on ^= (1 << 0);
				//lenta_on_off = 1;				
				IDC_A;
			break;
			
			case 0x0B:  // ��������� �����				
				lenta_on_off = 0;				
				IDC_A;
			break;
			
			case 0x10:  // ���.����� �� ������ �����			 
				metki_lenta = *(unsigned int *)(&msg.dta[1]);
				eep_word (ee_metki_lenta,metki_lenta);
				IDC_A;
			break;
			
			case 0x11:  // ���.������� �����
				diametr_lenta = *(unsigned int *)(&msg.dta[1]);				
				eep_word (ee_diametr_lenta, diametr_lenta);				
				IDC_A;
			break;

			case 0x12:  // ���.������ ����� �����
				coef_lenta = *(unsigned int *)(&msg.dta[1]);				
				eep_word (ee_coef_lenta, coef_lenta);				
				IDC_A;
			break;
			
			case 0x13:  // ���. ����� �� ������ ���. ����
				metki_koleso 		= *(unsigned int *)(&msg.dta[1]);				
				eep_word (ee_metki_koleso,metki_koleso);
				IDC_A;
			break;
			
			case 0x14:  // ���. �������� ������
				diametr_kolesa 		= *(unsigned int *)(&msg.dta[1]);				
				eep_word (ee_diametr_kolesa,diametr_kolesa);
				IDC_A;
			break;
			
			case 0x15:  // ���.����� �� ������ ��� ��� (���� ������ �������� )
				metki_vom 			= *(unsigned int *)(&msg.dta[1]);				
				eep_word (ee_metki_vom,metki_vom);
				IDC_A;
			break;
			
			case 0x16:  // ���. �����. ������� ����
				porog_vom 			= *(unsigned int *)(&msg.dta[1]);				
				eep_word (ee_porog_vom,porog_vom);
				IDC_A;
			break;
			
			case 0x17:  // ���.������������ ���������
				reduc_vom = *(unsigned int *)(&msg.dta[1]);
				eep_word (ee_reduc_vom,reduc_vom);
				IDC_A;
			break;
			
			case 0x18:  // ���.����������� �������� �������
				bunker_MIN 			= *(unsigned int *)(&msg.dta[1]);								
				eep_word (ee_bunker_MIN,bunker_MIN);
				IDC_A;				
			break;
			
			case 0x19:  // ���.������������ �������� �������
				bunker_MAX 			= *(unsigned int *)(&msg.dta[1]);
				eep_word (ee_bunker_MAX,bunker_MAX);
				IDC_A;
			break;
			
			case 0x1A:  // ���.������� �������� �������
				bunker_zapolnenie 			= *(unsigned int *)(&msg.dta[1]);
				if (bunker_zapolnenie > bunker_MAX)
					bunker_zapolnenie = bunker_MAX;
					
				IDC_A;
				
				eep_word(ee_bunker_zagruzka,bunker_zapolnenie);
				clear_probeg(ee_kgramm_b);
				kgramm_b=0;  gramm = 0;
				
			break;
			
			case 0x1B:  // ���.��������� ������� �����.	|��/��	
				rashod_kg_ga 				= *(unsigned int *)(&msg.dta[1]);				
				IDC_A;				
				eep_word (ee_rashod_kg_ga,rashod_kg_ga);				
			break;
			
			case 0x1C:  // ���.������ ����� �������	 �
				shirina_zahvata 			= *(unsigned int *)(&msg.dta[1]);
				
				// �������� ������������
				if (vid_udobrenie == 0) // ��� ��������� Granulador - 12 18 24
						{if (shirina_zahvata > 24)  
							shirina_zahvata = 24;}
				if (vid_udobrenie == 1) // ��� ��������� Nitrat - 12 18
						{if (shirina_zahvata > 18)  
							shirina_zahvata = 18;}
				if ((vid_udobrenie == 2) || (vid_udobrenie == 3) ) // ��� ������  - 12
						{if (shirina_zahvata > 12)  
							shirina_zahvata = 12;}
				
				if (shirina_zahvata < 12) shirina_zahvata = 12;
				
				IDC_A;
				
				eep_word (ee_shirina_zahvata,shirina_zahvata);
				
			break;
			
			case 0x1D:  // ���.���� ���������
				vid_udobrenie 				= *(unsigned int *)(&msg.dta[1]);
				
				// �������� ������������
				if (vid_udobrenie > 3) {vid_udobrenie = 0;}
				if (vid_udobrenie == 0) // ��� ��������� Granulador - 12 18 24
					{if (shirina_zahvata > 24)  
						{shirina_zahvata = 24; eep_word (ee_shirina_zahvata,shirina_zahvata);}}
				if (vid_udobrenie == 1) // ��� ��������� Nitrat - 12 18
					{if (shirina_zahvata > 18)  
						{shirina_zahvata = 18; eep_word (ee_shirina_zahvata,shirina_zahvata);}}
				if ((vid_udobrenie == 2) || (vid_udobrenie == 3) ) // ��� ������  - 12
					{if (shirina_zahvata > 12)  
						{shirina_zahvata = 12; eep_word (ee_shirina_zahvata,shirina_zahvata);}}
				
				IDC_A;
				
				eep_word (ee_vid_udobrenie,vid_udobrenie);
			break;
			
			case 0x1E:  // ��������: 0-auto    1 - GPS       2-6.0  3-8.0 4-10.0
				emul_speed	 			= *(unsigned int *)(&msg.dta[1]);				
				IDC_A;				
				eep_word (ee_emul_speed,emul_speed); 
			break;
			
			case 0x1F:  // ���.������������ �����
				calib_coeff	 			= *(unsigned int *)(&msg.dta[1]);
				if (calib_coeff < 50)
					{
					calib_coeff = 50;
					}
				if (calib_coeff > 200)
					{
					calib_coeff = 200;
					}	
				
				
				IDC_A;
				
				eep_word (ee_calib_coeff,calib_coeff); 
			break;
			
			case 0x50: // ����� ����� ����������
				square_s = way_s = time_s = kgramm_s = 0;
				gramm = time_sec_ws = metr2_x10_ws = way_metr_x10_ws = 0;
				
				square_w = way_w = time_w = kgramm_w = 0;
				gramm = time_sec_ws = metr2_x10_ws = way_metr_x10_ws = 0;
				
				clear_ee_s_flag = 10;// START EEP CLEAR
				clear_ee_w_flag = 11;
				IDC_A;
			break;
			
			case 0x51: //����� ������� ����������
				square_w = way_w = time_w = kgramm_w = 0;
				gramm = time_sec_ws = metr2_x10_ws = way_metr_x10_ws = 0;
				clear_ee_w_flag = 10;// START EEP CLEAR
				IDC_A;
			break;			
			
		default:
				GICR|=0x40;
			break;
			}
		}	// ifend
	else
		GICR|=0x40;		
		
	}// while end
	
return;	
}



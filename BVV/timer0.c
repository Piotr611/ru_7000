

//������ 2 ������������ ��� ��������� ������ �������� ������� ������������� 1�� � �������� ���������� ����������
//���������� ���������� ��������� ��������� ������� �������� ������� 


#define max_rpm_chanel 4
#define TICS_ONE_HZ 	2000
#define TICS_ONE_RPM 	TICS_ONE_HZ*60

#define TICS_FOR_OPEN 	TICS_ONE_HZ* 3.18
#define TICS_BEFORE		TICS_ONE_HZ* 0.08
#define TICS_AFTER		TICS_ONE_HZ* 0.08

#define MAX_PWM	0x3FF

#define devide_ms -(500000/4000)  //250//06 //��� ������ 16���


unsigned char detect_stop_rpm=0xff;  //���� �������� �� ����� ��������� ����� ������ 1 ��� � ������� ��� ������� ������
unsigned int  now_tics;				 //������� �����

unsigned char flag_shim;
unsigned char flag_shim_vom;

unsigned int one_sec;
unsigned int display_time,display_time_test,display1_time;
/*volatile*/ //unsigned char flag_sec,t_500,t_200;
/*volatile*/ //unsigned char flag_dysplay_test=0;

unsigned char StatusChannel1;   /* ���������� ��������� �������	*/

//unsigned int calcul_speed_coef,calcul_probeg_coef;		//������������� �������� ������� �������� � ������ �������� � ����������� 

//unsigned char table_naveski[]={25,35,63,88,112,137,160,190};

// new
volatile unsigned int can_msec;



void init_count(void)		/* ������������� ��������� ������		*/
{
  //DDRC |=0x11;
  PORTB|= (1<<PB1);//PORTC|=1;		//������ ������� �� �������

  DDRA =0; 
  PORTA=0;
  
  taho_chanel[1].pulse_metok_default = 1;
  taho_chanel[0].pulse_metok_default = metki_koleso;
  taho_chanel[3].pulse_metok_default = metki_lenta;  
  
  taho_chanel[2].pulse_metok_default=1; 


diametr_div_metki=(long)diametr_kolesa*100/metki_koleso;		//��������� �������� ������ � ���������� ����� ������������ ��� ��������
 
}  
  
 


//���������� ����� ��������
unsigned int delta_calc(RPM_SENSOR *temp)
{ return(now_tics-(temp->pulse_start));}

//���������  ������ � ������ ��������
void pulse_detect (RPM_SENSOR *temp)
{if (temp->pulse_metok==0) {temp->delta_pulse+=delta_calc(temp);temp->delta_pulse>>=1;
							   temp->pulse_start=now_tics;
							   temp->pulse_metok=temp->pulse_metok_default;
							   };  //�������� ������ ��������, ������� ���������� ����� �� 1 ������
							   temp->pulse_metok--;
							   temp->pulse_sec++;}


//������� ������������ �������� � �����
void convert_hz (RPM_SENSOR *temp)
{if (temp->delta_pulse) temp->taho_hz=TICS_ONE_HZ/temp->delta_pulse; else temp->taho_hz=0;}

//������� ������������ ������� � ������� � ������
void convert_vom (unsigned char chan){
							RPM_SENSOR *temp=&taho_chanel[chan];
							if (temp->delta_pulse) {temp->taho_rpm=((long)TICS_ONE_RPM*reduc_vom/(temp->delta_pulse*100)); //�������� �������� � ������
													if (detect_vom)	{if (temp->taho_rpm>porog_vom) detect_vom=0;};
						} else {temp->taho_rpm=0;
								//if ((temp->flag_no_control&1)==0) temp->flag_proskalz=work_now; else temp->flag_proskalz=0;
								}
}

//��������� ������� �������� ������� � �� �  
void convert_speed (unsigned char chan){
							RPM_SENSOR *temp=&taho_chanel[chan];
							if (temp->delta_pulse){
							convert_hz(temp);
							unsigned int Speed;
							
							unsigned long k=diametr_kolesa;//diametr_div_metki;
							k=k*2262/(((long)temp->delta_pulse)*metki_koleso);	//mm ��������� ������					
							k=(k+5)/10;
							Speed=k;
							if (Speed>999) Speed=999;
							temp->taho_rpm=Speed;}
							
							// �������� 
							if (emul_speed >= 2)  	{speed_now = emul_speed_table[emul_speed]; flag_probeg_calc++;}
							if (emul_speed == 1)    speed_now = ((GPS_actual_data)?(speed_now_GPS):(1));
							if (emul_speed == 0)	speed_now = taho_chanel[chan].taho_rpm;//speed_now_calc;
												
							//if (emul_speed_num)  speed_now=emul_speed; else speed_now=taho_chanel[0].taho_rpm;							
							
}


//��������� ������� �������� ����� � ������� � �� � ���  
void convert_lenta (unsigned char chan){
			
							RPM_SENSOR *temp=&taho_chanel[chan];
							if (temp->delta_pulse){
							convert_hz(temp);
							unsigned int Speed;
							
							unsigned long k=(long)diametr_lenta*coef_lenta;
							//k=k*314*temp->taho_hz/((long)metki_lenta*10000);	//mm ��������� ������		
							
							k=k*6283/(((long)temp->delta_pulse)*metki_lenta);	//mm ��������� ������					
							k=((long)k*popr_lenta+5000)/10000; 
							//k=(k+50)/100;
							//k=(k+5)/10;
							Speed=k;
							if (Speed>9999) Speed=9999;
							temp->taho_rpm=Speed;
							
							};
							speed_lenta = taho_chanel[chan].taho_rpm;  
							
							
}


void statistic_count (void){
if ((flag_probeg_calc) || (speed_now))  //��������� ��������� ����� 1 ���
						//��������� ������� 1 ��� � ������� ��� �������
				
				
				if (speed_lenta)				
				{		unsigned char ttmp=flag_probeg_calc;
						flag_probeg_calc-=ttmp;
						unsigned long k=diametr_kolesa*ttmp;

						unsigned long polovina=100*metki_koleso;  //�������� ��� �������
						if (left_on==0) polovina<<=1;
						if (right_on==0) polovina<<=1;
						if ((left_on==0)&(right_on==0)) {k=k*3142/(1000*metki_koleso);	//mm ��������� ������
															kolea_count = kolea_count + k;
															if (kolea_count>99999) kolea_count=0;
															goto no_work_now;};
						kolea_count=0;
						//k=k*taho_chanel[1].pulse_prev_sec;
						k=k*3142/polovina;	//cm ��������� ������
					
						k=k*shirina_zahvata;
						Square_sm +=k; 		square_w+=PROBEG_INC (ee_square_w,999999,&Square_sm);
						Square_sm_summ +=k;	square_s+=PROBEG_INC (ee_square_s,999999,&Square_sm_summ);
							
						no_work_now:;
						
				}	
				
						//gramm_w++;

#define H	698
#define HP	698*3.14
//698*3.14=2191
					if (work)
					//if (1) 
					{
						
					if ((calc_rachod_flag) || (speed_now) ) 
						{
						//calc_rachod_flag=0;
						
						unsigned int tmp=calc_rachod_flag;
						calc_rachod_flag-=tmp;
						
						unsigned long 	k=diametr_lenta; //������� ����� �� 
						//k=(((k*4288*vysota_naveski)/metki_lenta)*tmp*coef_lenta)/10000; //����� �� ���� ������� 4288
						k=(((k*HP*vysota_naveski)/metki_lenta*tmp*coef_lenta+50)/100*popr_lenta+50)/100;
						
						k=((k+50)/100*nasyp_massa+50)/100;
						//gramm_w=k;
						
						//k=k*shirina_zahvata;
						//k=k/600;
						//rashod_kg_min=k;// ������� ����������� ������ �� � ���
						calibr_massa_calc+=k;
						//k=(k*50)/6;		//������ � ������� �� �������  *50/60 �� ���� ������� � ����� ������
						//k=1000000;
						
						k=(k*kalibr_coef)/100;
						
						//if (left_on| right_on) gramm = gramm+k;
						if ((work) && (speed_lenta) && (speed_now) ) gramm = gramm+k;						
						//if ((left_on==0)&(right_on==0))goto no_secund_rashod;
						if ((!speed_lenta) || (!speed_now))goto no_secund_rashod;
						
						//��������� ������ 9.99999 ��
						if (PROBEG_INC (ee_kg1_w,99999,&gramm)) gramm_w+=10;
						//gramm_w++;			
						no_secund_rashod:;		


						
						};
						
						
						if ((speed_now) && (speed_lenta)) {
										unsigned int rashod_kg_ga_temp_last=rashod_kg_ga_temp;
										//rashod_kg_ga_temp=((unsigned long)taho_chanel[2].taho_rpm*233*nasyp_massa*vysota_naveski)/((long)speed_now*shirina_zahvata*250);
										rashod_kg_ga_temp=((unsigned long)speed_now*vysota_naveski*H/1000*nasyp_massa*10)/((long)speed_now*100/36*shirina_zahvata);
										rashod_kg_ga_temp=(rashod_kg_ga_temp+rashod_kg_ga_temp_last)>>1;
										rashod_kg_ga_temp=((long)rashod_kg_ga_temp*kalibr_coef)/100;									
										babax[0]++;
										} 
						else 			
										rashod_kg_ga_temp=0;
	
						// ������������ ��������� ��� OCR1A
						if (rashod_kg_ga_temp<rashod_kg_ga)
							{
							unsigned int delta_rashod=rashod_kg_ga-rashod_kg_ga_temp;
							if (delta_rashod>200)
								inc_OCR1A=7;//10
							else if (delta_rashod>100)
									inc_OCR1A=5;//7
							else if (delta_rashod>50)
									inc_OCR1A=4;
							else if (delta_rashod>20)
									inc_OCR1A=2;
							else 	inc_OCR1A=1;
								
							}
						if (rashod_kg_ga_temp>rashod_kg_ga)
							{
							unsigned int delta_rashod=rashod_kg_ga_temp-rashod_kg_ga;
							if (delta_rashod>200)
								inc_OCR1A=7;
							else if (delta_rashod>100)
									inc_OCR1A=5;
							else if (delta_rashod>50)
									inc_OCR1A=4;
							else if (delta_rashod>20)
									inc_OCR1A=2;
							else 	inc_OCR1A=1;
							
							}
						// -----^^^^^^������������ ��������� ��� OCR1A

						if ((rashod_kg_ga_temp<(rashod_kg_ga-5)) && (flag_shim==1)) 
							{
							flag_shim=0; 
							if ((OCR1A+inc_OCR1A)<MAX_PWM) 
								OCR1A+=inc_OCR1A; 
							else 
								OCR1A=MAX_PWM;} 
						if ((rashod_kg_ga_temp>(rashod_kg_ga+5)) && (flag_shim==1)) 
							{
							flag_shim=0; 
							if (OCR1A>=inc_OCR1A) 
								OCR1A-=inc_OCR1A; 
							else 
								OCR1A=0;
							};
						//if (imp_tranportera>taho_chanel[2].taho_hz*10) {if (OCR1A!=255) OCR1A++;} else {if (OCR1A!=0) OCR1A--;};
						
#define POROG_VOM_1	200
#define POROG_VOM_2	100
#define POROG_VOM_3	50
#define POROG_VOM_4	20
#define POROG_VOM_5	10

#define LEVEL_VOM_1	7
#define LEVEL_VOM_2	5
#define LEVEL_VOM_3	4
#define LEVEL_VOM_4	2
#define LEVEL_VOM_5	1
						unsigned int vom_now=taho_chanel[1].taho_rpm;
						// ������������ ��������� ��� OCR1B
						if (work) {
							OCR1A = 254;							
						} else {
							OCR1A=0;
						}
						if (!speed_now)
							OCR1A=0;
						/*
						if (vom_now<vom)
							{
							unsigned int delta_vom=vom-vom_now;
							if      (delta_vom>POROG_VOM_1)
									inc_OCR1B=LEVEL_VOM_1;//10
									
							else if (delta_vom>POROG_VOM_2)
									inc_OCR1B=LEVEL_VOM_2;//7
									
							else if (delta_vom>POROG_VOM_3)
									inc_OCR1B=LEVEL_VOM_3;
									
							else if (delta_vom>POROG_VOM_4)
									inc_OCR1B=LEVEL_VOM_4;
									
							else if (delta_vom>POROG_VOM_5)		
									inc_OCR1B=LEVEL_VOM_5;
								
							}
						if (vom_now>vom)
							{
							unsigned int delta_vom=vom_now-vom;
							if      (delta_vom>POROG_VOM_1)
									inc_OCR1B=LEVEL_VOM_1;//10
									
							else if (delta_vom>POROG_VOM_2)
									inc_OCR1B=LEVEL_VOM_2;//7
									
							else if (delta_vom>POROG_VOM_3)
									inc_OCR1B=LEVEL_VOM_3;
									
							else if (delta_vom>POROG_VOM_4)
									inc_OCR1B=LEVEL_VOM_4;
									
							else if (delta_vom>POROG_VOM_5)		
									inc_OCR1B=LEVEL_VOM_5;
							
							}
						// ----������������ ��������� ��� OCR1B

						if ((vom_now<(vom-5)) && (flag_shim_vom==1)) 
							{
							flag_shim_vom=0; 
							if ((OCR1B+inc_OCR1B)<MAX_PWM) 
								OCR1B+=inc_OCR1B; 
							else 
								OCR1B=MAX_PWM;} 
						if ((vom_now>(vom+5)) && (flag_shim_vom==1)) 
							{
							flag_shim_vom=0; 
							if (OCR1B>=inc_OCR1B) 
								OCR1B-=inc_OCR1B; 
							else 
								OCR1B=0;
							};
						
						*/
						
						
					}  
					else rashod_kg_ga_temp=0;
							
						
}




void test_pulse(void) 			/* function will get called when the timer overflows */
{
now_tics++;
unsigned char zzz;

unsigned char Stat1 = PINA&0x0F;//PINA&0x1E;
	zzz=(Stat1^StatusChannel1)&Stat1;StatusChannel1  = Stat1;  //�������� �����
	if (zzz) {
				if (bit_is_set(zzz,1)) {pulse_detect(&taho_chanel[0]);taho_chanel[0].flag_gnd=0;flag_probeg_calc++;} else taho_chanel[0].flag_gnd=1;
				if (bit_is_set(zzz,2)) {pulse_detect(&taho_chanel[1]);taho_chanel[1].flag_gnd=0;} else taho_chanel[1].flag_gnd=1;
				if (bit_is_set(zzz,3)) {pulse_detect(&taho_chanel[2]);taho_chanel[2].flag_gnd=0;} else taho_chanel[2].flag_gnd=1;
				if (bit_is_set(zzz,0)) {pulse_detect(&taho_chanel[3]);taho_chanel[3].flag_gnd=0;calc_rachod_flag++;} else taho_chanel[3].flag_gnd=1; 
			};
			
	

	//����  detect_stop_rpm ����� ������ ������� ���������� ��������� ����������� ������ �������. �� ������ ���� - 1 �����
	if (detect_stop_rpm!=0xff) {if (taho_chanel[detect_stop_rpm].delta_pulse) 
										{if (delta_calc(&taho_chanel[detect_stop_rpm])>TICS_ONE_HZ*3) {taho_chanel[detect_stop_rpm].pulse_metok=0;
																									  taho_chanel[detect_stop_rpm].delta_pulse=0;
																									  taho_chanel[detect_stop_rpm].taho_rpm=0;
																									  taho_chanel[detect_stop_rpm].taho_hz=0;
																									  };
										};
								
								
								taho_chanel[detect_stop_rpm].pulse_prev_sec=taho_chanel[detect_stop_rpm].pulse_sec;
								taho_chanel[detect_stop_rpm].pulse_sec=0;
								
								if (++detect_stop_rpm>=max_rpm_chanel) detect_stop_rpm=0xff;
								};
				
				
}





void init_pwm_out(void){
	PORTD&=~_BV(PD5);
	DDRD|=_BV(PD5)|_BV(PD4);;  //������������ 2 ������ PWM
	OCR1A=0;//����� ������ 1
	TCCR1A=0x0; //pinb pinc on fast pwm 8 bit 0x79;   ������ ��� B+C
	TCCR1B=0x0B; // 8 bit clk
}




//������������� �������0
void init_timer0(void) {
  TCCR0 = _BV (CS01) | _BV (CS00);				/* TCCR0: �������� �� ������� CK / 64  */
  TCNT0 = (char)devide_ms;   
  TIMSK|=(_BV (TOIE0) );   			/* TOIE1: Timer/Counter0 Overflow Interrupt Enable */
  init_count();
  init_pwm_out();
}




SIGNAL (SIG_OVERFLOW0) {        //���������� ���������� ��� ������������� ������� 0
//sei(); 
TCNT0 += devide_ms;

test_pulse();
////////////////////////////////

#define MSEC_CAN_STEP	100
if 	(	   (can_msec!=(MSEC_CAN_STEP*1)) && (can_msec!=(MSEC_CAN_STEP*2)) \
&& (can_msec!=(MSEC_CAN_STEP*3)) && (can_msec!=(MSEC_CAN_STEP*4)) \
&& (can_msec!=(MSEC_CAN_STEP*5)) && (can_msec!=(MSEC_CAN_STEP*4)) \
&& (can_msec!=(MSEC_CAN_STEP*7)) && (can_msec!=(MSEC_CAN_STEP*4)) \
&& (can_msec!=(MSEC_CAN_STEP*9)) && (can_msec!=(MSEC_CAN_STEP*10)) \
)
{
can_msec++;
if (can_msec > 1002)
can_msec=0;

}


////////////////////////////////
  
if ((one_sec%(TICS_ONE_HZ/10))==0)
	{
	flag_shim=1;
	flag_shim_vom=1;
	}
if (++one_sec>=TICS_ONE_HZ) {detect_stop_rpm=0;  //��������� ���������� ��������� �� 1 ���
							  one_sec=0;							  
							};

if (++display_time_test>=99) {display_time_test=0; 
								if (rpm_flag_transmit==0) rpm_flag_transmit=1;								
								};


if (can_time_busy) can_time_busy--;



wdt_reset();
}

